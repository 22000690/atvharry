//
//  ViewController.swift
//  harrythiago
//
//  Created by COTEMIG on 01/03/39 ERA1.
//


import UIKit
import Alamofire
import Kingfisher

struct Ator: Decodable {
    let name: String
    let actor: String
    let image: String
}



class ViewController: UIViewController, UITableViewDataSource {
    
    var ListaAtor:[Ator]?
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListaAtor?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! MyCell
        let ator = ListaAtor![indexPath.row]
        
        cell.Ator.text = ator.actor
        cell.Personagem.text = ator.name
        cell.Imagem.kf.setImage(with: URL(string: ator.image))
        return cell
    }
    

    
    @IBOutlet weak var TableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        data()
        self.TableView.dataSource = self
    }
    
    private func data(){
        AF.request("https://hp-api.herokuapp.com/api/characters")
        .responseDecodable(of:[Ator].self){
        response in
        if let ListaAtor = response.value{
            self.ListaAtor = ListaAtor
        }
            self.TableView.reloadData()
    }


}
}
